

export const primary='#4ABAD1'
export const secondary='#F0A42D'
export const light='#fff'
export const tertiary='#D1514A'
export const dark='#000000'
export const grey='#BCB3B3'