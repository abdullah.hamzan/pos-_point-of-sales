import React, { useState } from 'react'
import { useSelector,useDispatch } from 'react-redux'
import styled from 'styled-components'
import { ResetCart } from '../store/actions/product'

import Button from './Button'

const Box=styled.div`
position:fixed;
bottom:0;
width:450px;
hight:229px;
box-shadow:1px 1px 10px 1px #ccc;
padding:0.4rem;
padding-bottom:23;
align-items:center;
justify-content:center;
margin-bottom:23;


`
const Total=styled.div`
display:flex;
justify-content:space-between;
margin-top:16px;
margin-left:17px;

`
const Pay=styled.div`
display:flex;
justify-content:space-between;
input[type=number]{
    border:none;
    border-bottom:1px solid #000;
    font-wight:bold;
    text-align:right;
    &:focus{
        outline:none;
    }
    &::-webkit-linner-spin-button;
    &::-webkit-outer-spin-button{
        -webkit-appearance:none
    }


}
margin-top:16px;
margin-left:17px;

`
const Change=styled.div`
display:flex;
justify-content:space-between;
margin-top:16px;
margin-left:17px;
margin-bottom:55px;


`
const Nominal=styled.div`
display:flex;
justify-content:space-between;


`
const CardButton=styled.div`

display:flex;
justify-content:space-between;

`


const CalculateBox = () => {
    const dispatch=useDispatch()

    const carts=useSelector(state=>state.product.cart)
    const total=carts.reduce((totalValue,currentValue)=>totalValue+currentValue.price,0)
   const [pay,setPay]=useState("")
   const [change,setChange]=useState("")
   const HanleOnCheng=(e)=>{

    setPay(e.target.value);
    
  

   }

   const CalculateChange=()=>{
       if (pay>total){

        return setChange(pay-total)
       }
      
   }

   const ResetDataProduct=()=>{
       dispatch(ResetCart())
   }
   

    return (
        <Box>
        <Total>
            <h4>Total</h4>

            <Nominal>{total}</Nominal>
        </Total>
        <Pay>
            <h5>Jumlah bayar</h5>
            <input type="number" onChange={HanleOnCheng} value={pay} />
      
        </Pay>
        <Change>
        <h5>Kembalian</h5>
            <Nominal>{change}</Nominal>
        </Change>
        <CardButton>
        <Button  action={ResetDataProduct}/>
        <Button primary action={CalculateChange}  />

        </CardButton>

            
        </Box>
    )
}

export default CalculateBox
