
import React from 'react'
import styled from 'styled-components'

const CounterStyled=styled.div`
width:1rem;
background:${props=>props.theme.secondary};
color:${props=>props.theme.light};
border-radius:100px;
height:1rem;
display:flex;
justify-content:center;
align-items:center;
cursor:pointer;

`

const Counter = ({inc,tambah,kurang}) => {

    if (inc){
    return (
        <CounterStyled  onClick={tambah}>
            +
        </CounterStyled>
    )}
    else {
        return (
            <CounterStyled onClick={kurang}>
                -
            </CounterStyled>
        )
    }
}

export default Counter
