
import React, { useState } from 'react'
import styled from 'styled-components'
import { Decrement, Increment, RemoveCart } from '../store/actions/product'
import Counter from './Counter'
import {useDispatch} from 'react-redux'

const Cart = styled.div`
display:flex;
width:95%;
justify-content:space-between;

align-items:center;
height:3rem;
padding:0 3rem;
box-shadow:1px 1px 10px 1px #ccc;
margin-top:8px;

`

const CounterContainer=styled.div`
display:flex;
justify-content:space-between;
align-items:center;


`
const ItemName=styled.div`
 color:${props=>props.theme.dark};
 font-size:20px;


`
const Count=styled.div`
font-size:20px;
color:${props=>props.theme.dark};
margin:0 8px;


 `

const CardItem = ({cart}) => {
    const dispatch=useDispatch()
    const [count,setCount]=useState(1)
    const increment=(id)=>{

        setCount(count+1)
        dispatch(Increment(id))

    }
    const decrement=(id)=>{

        if (count >1 ){
            dispatch(Decrement(id))
            setCount(count-1)
           
            
        }else if (count===1) {
            dispatch(RemoveCart(id))

        }

       

    }


    return (
        <Cart>
        <ItemName>{cart.name}</ItemName>
        <CounterContainer>
            <Counter inc tambah={()=>increment(cart.id)}  />
            <Count>{count}</Count>
            <Counter kurang={()=>decrement(cart.id)} />
        </CounterContainer>
        <ItemName>{cart.price}</ItemName>
            
        </Cart>
    )
}

export default CardItem
