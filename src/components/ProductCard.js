
import React from 'react'
import styled from 'styled-components'
import { AddCart } from '../store/actions/product'
import { useDispatch } from 'react-redux'

const Card=styled.div`
width:17%;
height:12rem;
cursor:pointer;
`
const CardImg=styled.img`
width:134px;
height:134px;

`
const NamePrice=styled.div`
margin-top:7px;
display:flex;
justify-content:'space-between';

`


const ProductCard = ({item}) => {

    const dispatch=useDispatch();

    const addCart=(id)=>{

        dispatch(AddCart(id))

        console.log("id:",id)
    }
    return (
        <Card onClick={()=>addCart(item.id)}>
        <CardImg src={item.image.default} />
        <NamePrice >
            <p>{item.name}</p>
            <p>{item.price}</p>
        </NamePrice>
        </Card>
    )
}

export default ProductCard
