
import React from 'react'
import styled from 'styled-components'

const MyButton=styled.button`
height:1.7rem;
width:6rem;
align-items:center;
color:${props=>props.theme.light};
border:none;
padding:0.2rem 0.5rem;
cursor:pointer;
&:focus:{
    outline:none;
}

`
const SecondaryButton=styled(MyButton)`
background:${props=>props.theme.tertiary}

`
const PrimaryButton=styled(MyButton)`
background:${props=>props.theme.primary}

`


const Button = ({primary,action}) => {

    if (primary){
        return (
           
            <PrimaryButton onClick={action} >SELESAI</PrimaryButton>
           
        )
    }else {

        return (
       
        <SecondaryButton onClick={action}>CANCEL</SecondaryButton>
        )
       
    }

    
}

export default Button
