
import React from 'react'
import styled from 'styled-components'

const Head=styled.div`
height:8vh;
justify-content:center;
align-items:center;
display:flex;
background:${props=>props.theme.primary};


`
const Title=styled.h1`
 color:${props=>props.theme.light}

`


const Header = () => {
    return (
        <Head>
            <Title>KAASIR</Title>
            
        </Head>
    )
}

export default Header
