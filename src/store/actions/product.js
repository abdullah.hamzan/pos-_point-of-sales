
export const AddCart=(id)=>{

    return {
        type:"ADD_CART",
        payload:id
    }
}

export const Increment=(id)=>{

    return {
        type:"INCREMENT",
        payload:id
    }
}

export const Decrement=(id)=>{

    return {
        type:"DECREMENT",
        payload:id
    }
}

export const RemoveCart =(id)=>{
    return {
        type:"REMOVE",
        payload:id
    }
}
export const ResetCart=()=>{
    return {
        type:"RESET"

    }
}