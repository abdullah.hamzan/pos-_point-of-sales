import { combineReducers} from 'redux'
import productReducers from './productReducer'


export const rootReducer= combineReducers({
    product:productReducers
})