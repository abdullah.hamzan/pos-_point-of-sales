import { products } from "../utils/data";


// state
const initialState={
    product:products,
    cart:[]
}


const productReducers=(state=initialState,action)=>{
    const {type,payload}=action

    switch (type){
        case "ADD_CART":
            const itemIncart=state.cart.find(item=>item.id===payload);
            const newItem=state.product.find(item=>item.id===payload);

            if (!itemIncart){
                return {
                    ...state,
                    cart:[...state.cart,newItem]
                }
            }else {
                return state;
            }
        case "INCREMENT":
            const originalProduct=state.product.find(item=>item.id===payload).price
            const inCart=state.cart.map(item=>{
                if (item.id===payload){
                    return {
                        ...item,
                        price:item.price+originalProduct
                    }
                }else {
                    return item
                }
            });

            return {
                ...state,
                cart:inCart
            }

        case "DECREMENT":

            const originalPrice=state.product.find(item=>item.id===payload).price
            const inCartPrice=state.cart.map(item=>{
                if (item.id===payload){
                    return {
                        ...item,
                        price:item.price-originalPrice
                    }
                }else {
                    return item
                }
            });

            return {
                ...state,
                cart:inCartPrice
            }

        case "REMOVE":
            return {
                ...state,
                cart:state.cart.filter(item=>item.id !==payload)
            }

        case "RESET":
            return {
                ...state,
                cart:[]
            }

            

        default:
            return state


    }
    

}

export default productReducers;