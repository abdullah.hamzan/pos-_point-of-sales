import './App.css';
import React from 'react';
import styled,{ThemeProvider} from 'styled-components';
import logo from './logo.svg'
const theme={
  primary:'red',
  seondary:"blue"
}

const Container=styled.div`
  text-align:center;
`
const Head=styled.h1`
color:${props=>props.theme.secondary}

`
const Brand=styled.img`
height:10vh;
margin-top:2rem;

`
const Button=styled.button`
color:#fff;
background:${props=>props.primary ? "green":"blue"};
padding:0.5rem 1rem;
border:none;
margin:0 1rem;
`
const TomatoButton=styled(Button)`

background:orange

`
const Wrapper=styled.div`
background:yellow;
&.test{
  background:red
}

`

const AnotherButton=styled.button`
 
   ${props=>{
     switch (props.variant) {
       case "primary":
         return `background:blue`;
        case "secondary":
          return `background:green`
        default:
          return `background:red`
       
     }
   }}

`


const App=()=> {
  return (
    <ThemeProvider theme={theme}>
   <Container>
     <Brand src={logo} />
     <Head>Component styled</Head>
     <Button >primary</Button>
     <Button primary >primary</Button>
     <TomatoButton>My Tomato</TomatoButton>
     <Wrapper>biasa</Wrapper>
     <Wrapper className="test">with class</Wrapper>
     <AnotherButton variant="primary">primary</AnotherButton>
     <AnotherButton variant="secondary">secondary</AnotherButton>
     <AnotherButton >default</AnotherButton>
   </Container>
   </ThemeProvider>
  );
}

export default App;
